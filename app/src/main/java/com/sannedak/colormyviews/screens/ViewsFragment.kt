package com.sannedak.colormyviews.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.ClickableText
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.font.ResourceFont
import androidx.compose.ui.text.font.fontFamily
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.ui.tooling.preview.Preview
import com.sannedak.colormyviews.R
import com.sannedak.colormyviews.ui.*

class ViewsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = ComposeView(requireContext()).apply {
        setContent {
            ColorMyViewsTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    ColorMyViews()
                }
            }
        }
    }

    @Composable
    private fun ColorMyViews() {
        val marginWide = 16.dp
        val marginHalf = 8.dp

        Column {
            TopAppBar(title = { Text(text = stringResource(id = R.string.app_name)) })
            ConstraintLayout(modifier = Modifier.fillMaxSize()) {
                val (box1, box2, box3, box4, box5,
                    textInfo, textLabel, redButton, yellowButton, greenButton) = createRefs()
                createVerticalChain(box3, box4, box5, chainStyle = ChainStyle.SpreadInside)
                createHorizontalChain(
                    redButton,
                    yellowButton,
                    greenButton,
                    chainStyle = ChainStyle.Spread
                )

                val boxOneModifier = Modifier
                    .constrainAs(box1) {
                        start.linkTo(parent.start, margin = marginWide)
                        end.linkTo(parent.end, margin = marginWide)
                        top.linkTo(parent.top, margin = marginWide)
                        width = Dimension.fillToConstraints
                    }
                val boxTwoModifier = Modifier
                    .constrainAs(box2) {
                        start.linkTo(parent.start, margin = marginWide)
                        top.linkTo(box1.bottom, margin = marginWide)
                        bottom.linkTo(textLabel.top, margin = marginHalf)
                        linkTo(box1.bottom, textLabel.top, topMargin = marginWide, bias = 0.0F)
                    }
                    .size(130.dp)
                val boxThreeModifier = Modifier
                    .constrainAs(box3) {
                        start.linkTo(box2.end, margin = marginWide)
                        end.linkTo(parent.end, margin = marginWide)
                        top.linkTo(box2.top)
                        bottom.linkTo(box4.top)
                        width = Dimension.fillToConstraints
                    }
                val boxFourModifier = Modifier
                    .constrainAs(box4) {
                        start.linkTo(box2.end, margin = marginWide)
                        end.linkTo(parent.end, margin = marginWide)
                        top.linkTo(box3.bottom, margin = marginWide)
                        bottom.linkTo(box5.top, margin = marginWide)
                        width = Dimension.fillToConstraints
                    }
                val boxFiveModifier = Modifier
                    .constrainAs(box5) {
                        start.linkTo(box2.end, margin = marginWide)
                        end.linkTo(parent.end, margin = marginWide)
                        top.linkTo(box4.bottom)
                        bottom.linkTo(box2.bottom)
                        width = Dimension.fillToConstraints
                    }
                val textInfoModifier = Modifier
                    .constrainAs(textInfo) {
                        start.linkTo(textLabel.end, margin = marginWide)
                        end.linkTo(parent.end, margin = marginWide)
                        top.linkTo(box2.bottom, margin = marginWide)
                        bottom.linkTo(parent.bottom, margin = marginHalf)
                        linkTo(box5.bottom, parent.bottom, bias = 0.12F)
                        linkTo(textLabel.end, parent.end, endMargin = marginWide, bias = 1F)
                    }
                val textLabelModifier = Modifier
                    .constrainAs(textLabel) {
                        start.linkTo(parent.start, margin = marginWide)
                        baseline.linkTo(textInfo.baseline)
                    }
                val redButtonModifier = Modifier
                    .constrainAs(redButton) {
                        start.linkTo(parent.start)
                        end.linkTo(yellowButton.start)
                        baseline.linkTo(yellowButton.baseline)
                    }
                val yellowButtonModifier = Modifier
                    .constrainAs(yellowButton) {
                        start.linkTo(redButton.end)
                        end.linkTo(greenButton.start)
                        top.linkTo(textLabel.bottom, margin = marginHalf)
                        bottom.linkTo(parent.bottom, margin = marginWide)
                        linkTo(
                            textLabel.bottom,
                            parent.bottom,
                            bottomMargin = marginWide,
                            bias = 1.0F
                        )
                    }
                val greenButtonModifier = Modifier
                    .constrainAs(greenButton) {
                        start.linkTo(yellowButton.end)
                        end.linkTo(parent.end)
                        baseline.linkTo(yellowButton.baseline)
                    }

                ColoredBox(
                    newColor = Color.DarkGray,
                    modifier = boxOneModifier,
                    boxText = stringResource(id = R.string.box_one)
                )
                ColoredBox(
                    newColor = Color.Gray,
                    modifier = boxTwoModifier,
                    boxText = stringResource(id = R.string.box_two)
                )
                MultiColorBox(
                    boxName = stringResource(id = R.string.box_three),
                    boxColor = holoGreenLight,
                    boxModifier = boxThreeModifier,
                    buttonName = stringResource(id = R.string.button_red),
                    buttonColor = red,
                    buttonModifier = redButtonModifier
                )
                MultiColorBox(
                    boxName = stringResource(id = R.string.box_three),
                    boxColor = holoGreenDark,
                    boxModifier = boxFourModifier,
                    buttonName = stringResource(id = R.string.button_yellow),
                    buttonColor = yellow,
                    buttonModifier = yellowButtonModifier
                )
                MultiColorBox(
                    boxName = stringResource(id = R.string.box_three),
                    boxColor = holoGreenLight,
                    boxModifier = boxFiveModifier,
                    buttonName = stringResource(id = R.string.button_green),
                    buttonColor = green,
                    buttonModifier = greenButtonModifier
                )
                Text(
                    text = stringResource(id = R.string.tap_the_boxes_and_buttons),
                    modifier = textInfoModifier
                )
                Text(
                    text = stringResource(id = R.string.how_to_play),
                    style = labelText,
                    modifier = textLabelModifier
                )
            }
        }
    }

    @Composable
    private fun MultiColorBox(
        boxName: String,
        boxColor: Color,
        boxModifier: Modifier,
        buttonName: String,
        buttonColor: Color,
        buttonModifier: Modifier
    ) {
        /**
         * States: 0 - initial
         *         1 - tap on button
         *         2 - tap on box
         */
        val tapped = remember { mutableStateOf(0) }

        when (tapped.value) {
            0 -> ColoredByButtonBox(
                originalColor = Color.White,
                newColor = boxColor,
                modifier = boxModifier,
                boxText = boxName
            )
            1 -> ChangeableBox(
                newColor = buttonColor,
                modifier = boxModifier,
                boxText = boxName,
                onClick = { tapped.value = 2 }
            )
            2 -> ColoredByButtonBox(
                originalColor = boxColor,
                newColor = boxColor,
                modifier = boxModifier,
                boxText = boxName
            )
        }

        Button(onClick = { tapped.value = 1 }, modifier = buttonModifier) {
            Text(
                text = buttonName,
                fontFamily = fontFamily(ResourceFont(resId = R.font.roboto))
            )
        }
    }

    @Composable
    private fun ChangeableBox(
        newColor: Color,
        modifier: Modifier,
        boxText: String,
        onClick: (Int) -> Unit
    ) {
        Surface(color = newColor, modifier = modifier) {
            ClickableText(
                onClick = onClick,
                text = AnnotatedString(text = boxText),
                style = whiteBoxText
            )
        }
    }

    @Composable
    private fun ColoredByButtonBox(
        originalColor: Color,
        newColor: Color,
        modifier: Modifier,
        boxText: String
    ) {
        val changed = remember { mutableStateOf(false) }

        if (changed.value) {
            Surface(color = newColor, modifier = modifier) {
                Text(text = boxText, style = whiteBoxText)
            }
        } else {
            Surface(color = originalColor, modifier = modifier) {
                ClickableText(
                    onClick = { changed.value = true },
                    text = AnnotatedString(text = boxText),
                    style = whiteBoxText
                )
            }
        }
    }

    @Composable
    private fun ColoredBox(newColor: Color, modifier: Modifier, boxText: String) {
        val changed = remember { mutableStateOf(false) }

        if (changed.value) {
            Surface(color = newColor, modifier = modifier) {
                Text(text = boxText, style = whiteBoxText)
            }
        } else {
            Surface(color = Color.White, modifier = modifier) {
                ClickableText(
                    onClick = { changed.value = true },
                    text = AnnotatedString(text = boxText),
                    style = whiteBoxText
                )
            }
        }
    }

    @Preview
    @Composable
    private fun DefaultPreview() {
        ColorMyViewsTheme {
            ColorMyViews()
        }
    }
}
