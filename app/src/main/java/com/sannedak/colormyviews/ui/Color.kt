package com.sannedak.colormyviews.ui

import androidx.compose.ui.graphics.Color

val purple200 = Color(0xFFBB86FC)
val purple500 = Color(0xFF6200EE)
val purple700 = Color(0xFF3700B3)
val teal200 = Color(0xFF03DAC5)

val holoGreenLight = Color(0xFF99CC00)
val holoGreenDark = Color(0xFF669900)
val red = Color(0xFFE54304)
val yellow = Color(0xFFFFC107)
val green = Color(0xFF12C700)